const connect = require('connect')
const http = require('http')
const ShareDB = require('sharedb')
const ShareDBMingoMemory = require('sharedb-mingo-memory')
const WebSocketJSONStream = require('websocket-json-stream')
const WebSocket = require('ws')

const share = ShareDB({
  db: new ShareDBMingoMemory()
})
const app = connect()
const server = http.createServer(app)
const wss = new WebSocket.Server({
  server
})

wss.on('connection', (ws, req) => {
  const stream = new WebSocketJSONStream(ws)
  share.listen(stream)
})

const connection = share.connect()
connection.createFetchQuery('todos', {}, {}, (error, results) => {
  if (error) {
    throw error
  }

  if (results.length === 0) {
    const todos = [
      { label: 'Item One', completed: false },
      { label: 'Item Two', completed: false },
      { label: 'Item Three', completed: false }
    ]

    todos.forEach((todo, index) => {
      const doc = connection.get('todos', ''+index);
      doc.create(todo)
    })
  }
})

server.listen(3001)
